// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter.controllers',[]);
angular.module('starter', ['ionic','starter.controllers'])
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
}).config(function ($stateProvider) {

    $stateProvider
        .state('home',{
          url:'/home',
          templateUrl:'templates/home.html',
          controller:'HomeController'
        })
        .state('main',{
          url:'/main',
          templateUrl:'templates/main.html'
        })
        .state('client',{
          abstract:true,
          url:'/client',
          template:'<ui-view/>'
        })
        .state('client.checkout',{
          url:'/checkout',
          templateUrl:'templates/client/checkout.html',
          controller:'ClientCheckoutController'
        })
        .state('client.checkout_item_datail',{
          url:'/checkout/detail/:index',
          templateUrl:'templates/client/checkout_item_detail.html',
          controller:'ClientCheckoutDetailController'
        })
        .state('client.view_products',{
          url:'/view_products',
          templateUrl:'templates/client/view_products.html',
          controller:'ClientViewProductController'
      });

});
